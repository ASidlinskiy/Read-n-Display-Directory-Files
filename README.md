Read-n-Display-Directory-Files
==============================


This is a script that I wrote in school to list existing files in a given directory

and output them in a table providing name of the file, owner permissions, and size.


Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
